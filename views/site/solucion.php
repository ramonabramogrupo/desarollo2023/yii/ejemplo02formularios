<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model
]);
?>

<div>Suma: <?= $model->suma() ?></div>
<div>Vocales: <?= $model->vocales1() ?></div>