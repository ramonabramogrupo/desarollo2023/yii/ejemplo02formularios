<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "numero1",
        "numero2",
        [
            "attribute" => "Suma de los numeros",
            "value" => function($model){
                return  $model->suma();
            }
        ],
    ]
]);
?>

<div>Suma: <?= $model->suma() ?></div>
<div>Resta: <?= $model->resta() ?></div>
<div>producto: <?= $model->producto() ?></div>
<div>cociente: <?= $model->operacion("cociente") ?></div>