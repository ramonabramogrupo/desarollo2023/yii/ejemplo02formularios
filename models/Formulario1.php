<?php
namespace app\models;

use yii\base\Model;
class Formulario1 extends Model{
/*
    tenemos que crear los campos del formulario
*/
public ?int $numero1=null;
public ?int $numero2=null;
public function attributeLabels(){
    return [
        "numero1" => "Numero 1",
        "numero2" => "Numero 2",
    ];
}

public function rules(){
    return [
        [['numero1','numero2'],'required','message'=>'El campo {attribute} es obligatorio'],
        //[['numero1'],'integer','max'=>100,'min'=>0],
        //[['numero2'],'integer'],
        [['numero1','numero2'],'integer'],
        [['numero1'],function($atributo){
            if(!($this->$atributo>0 && $this->$atributo<100)){
                    return $this->addError($atributo,"El numero1 debe estar entro 0 y 100");
            }
        }],
        [['numero2'], 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number'],
    ];
}

public function suma(){
    return $this->numero1+$this->numero2;
}

public function resta(){
    return $this->numero1-$this->numero2;
}
public function producto(){
    return $this->numero1*$this->numero2;
}
public function cociente(){
    return $this->numero1/$this->numero2;
}

function operacion($operador){
    //$this->$operador();
    switch ($operador) {
        case 'suma':
            return $this->suma();
        case 'resta':
            return $this->resta();
        case 'producto':
            return $this->producto();
        case 'cociente':
            return $this->cociente();
    }
}

}
