<?php
namespace app\models;

use yii\base\Model;
class Formulario extends Model{
/*
    tenemos que crear los campos del formulario
*/
public ?int $numero1=null;
public ?int $numero2=null;
public ?string $descripcion=null;

public function attributeLabels(){
    return [
        "numero1" => "Numero 1",
        "numero2" => "Numero 2",
        "descripcion" => "Descripcion"
    ];
}

public function rules(){
    return [
        [['numero1','numero2','descripcion'],'required'],
    ];
}

public function suma(){
    return $this->numero1+$this->numero2;
}


/**
 * vocales
 * @return int
 * @deprecated 
 */
public function vocales(){
    $vector=str_split($this->descripcion);
    $vocales=0;
    foreach ($vector as $caracter) {
        switch (strtolower($caracter)) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':                                               
            case 'u':                        
                $vocales++;
                break;
        }
    }
    return $vocales;
}

public function vocales1(){
    $vector=str_split($this->descripcion);
    $vocales=0;
    foreach ($vector as $caracter) {
        if(in_array(strtolower($caracter),['a','e','i','o','u'])){
            $vocales++;
        }
    }
    return $vocales;
}
}
